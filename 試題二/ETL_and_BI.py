#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pymongo
import pandas as pd
import dataframe_image as di
import datetime
import warnings
warnings.filterwarnings('ignore')
import re
dataset_path = 'dataset_building_ce.gzip'


# In[2]:


# 避免相同資料重覆計算，以下為Distinct
df = pd.read_parquet(dataset_path , columns =  ['city', 'area', 'road', '使照號碼', '建照號碼', '起造人', '設計人',
       '建築地址(代表號)', '發照日期'])
df.reset_index(inplace = True)
df = df.groupby(['city', 'area', 'road', '使照號碼', '建照號碼', '起造人', '設計人',
       '建築地址(代表號)', '發照日期']).max()
df.reset_index(inplace = True)
df= df.iloc[:,:-1]

# 產出使照數量，並製成長條圖
cnt_df = df.groupby(['city','area'])[['使照號碼']].count()
cnt_df.to_parquet('REPORT_zone_certification_amount.gzip' , compression = 'gzip')
cnt_df_style = cnt_df.style.bar('使照號碼').set_caption('各區發照數量分佈')
di.export(cnt_df_style,'zone_certification_amount.png')


# In[3]:


# 為判斷發照日期長度，對此進行處理
df[['發照日期_Y','發照日期_M','發照日期_D']] = df['發照日期'].str.split('/' , expand = True)
df['發照日期_Y'] = df.apply(lambda x : int(x['發照日期_Y'])+1911 if len(x['發照日期_Y']) == 3 and re.match(r'^\d{3}$',x['發照日期_Y'])                         else x['發照日期_Y'] , axis =1 )
# 只有年份的資料補上"月日"01/01
df['發照日期_Y'].fillna('', inplace = True)
df['發照日期_M'].fillna('01', inplace = True)
df['發照日期_D'].fillna('01', inplace = True)
df['發照日期'] = df.apply(lambda x : f"{x['發照日期_Y']}/{x['發照日期_M']}/{x['發照日期_D']}" ,axis= 1)
df.drop(['發照日期_Y','發照日期_M','發照日期_D'] ,axis= 1 , inplace = True)

# 切分符合"19XX年至202X年內正確日期格式"與"非正確日期格式資料"
year_pattern = re.compile(r"^(20[012]{1}\d{1}|19\d{2})\/([01]{1}\d{1})\/([0123]{1}\d{1})$")
df['is_correct_date'] = df.apply(lambda x : 1 if len(x['發照日期']) == 10 and re.match(year_pattern,x['發照日期']) else 0 , axis = 1 )

# 錯誤日期資料
df_e = df[df['is_correct_date'] == 0].iloc[:,:-1]
df_r = df.drop(df_e.index)

# 正確日期資料，並開始計算最久建照日期與2020/11/1的相隔年數
df_r = df_r.iloc[:,:-1]
df_r['dt_發照日期'] = pd.to_datetime(df_r['發照日期'])
df_r['delta_D'] = (pd.to_datetime('2020/11/1')-df_r['dt_發照日期'])
df_r['max_use_time'] = df_r.groupby(['city','area'])['delta_D'].transform('max')
# 保留各區"建照日期與2020/11/1的相隔日數最久"資料（可能為多筆）
df_agg_r = df_r[df_r['delta_D'] == df_r['max_use_time']]

# 將差異"日"換算差異"年"
df_agg_r['delta_D'] = df_agg_r['delta_D'].astype('string')
df_agg_r['delta_D'] = df_agg_r.apply(lambda x : re.findall(r'(\-{0,1}\d+) days',x['delta_D'])[0]                                 if re.findall(r'(\-{0,1}\d+) days',x['delta_D']) != []                                else 'unreco', axis = 1)
df_agg_r['delta_D'] = df_agg_r['delta_D'].astype('float')
df_agg_r['使照已發出時間(年)'] = df_agg_r['delta_D']/365

# 依已發照時間"降冪排序"
df_agg_r.sort_values('使照已發出時間(年)' , ascending= False , inplace = True)
df_agg_r.drop(['delta_D','max_use_time','dt_發照日期'],axis = 1 , inplace = True)
df_agg_r.to_parquet('REPORT_zone_certification_data.gzip' , compression = 'gzip')
# 產出各區使用已發出最久資料，製成長條圖
di.export(df_agg_r.style.bar('使照已發出時間(年)',vmin = 0).set_caption('各區已發出最久建照'),'one_certification_data.png')


# In[7]:


# 產出最久"設計人"名稱下所有使照資料
longest_designer = df_agg_r['設計人'].iloc[0]
df[df['設計人']==longest_designer].iloc[:,:-1].to_parquet('REPORT_oldest_designer_data.gzip' , compression = 'gzip')
df[df['設計人']==longest_designer].iloc[:,:-1].to_csv('oldest_designer_data.csv' , index = False)

# 產出最久"起造人"名稱下所有使照資料
longest_builder = df_agg_r['起造人'].iloc[0]
df[df['起造人']==longest_builder].iloc[:,:-1].to_parquet('REPORT_oldest_builder_data.gzip' , compression = 'gzip')
df[df['起造人']==longest_builder].iloc[:,:-1].to_csv('oldest_builder_data.csv' , index = False)


# In[5]:


# 計算執行日與發照日差異天數
df_r['building_age'] =  pd.to_datetime(str(datetime.date.today()))- df_r['dt_發照日期']
df_r['building_age'] = df_r['building_age'].astype('string')
df_r['building_age'] = df_r['building_age'].str.replace('days','').astype('int')

# 分組為'未滿10Y','10Y-20Y','21Y-30Y','30Y以上'後計算各組個數
df_r['building_age_group']  = pd.cut(df_r['building_age'] , bins = [0,3650,7300,10950,99999] , labels = ['未滿10Y','10Y-20Y','21Y-30Y','30Y以上'])
cnt_df = df_r.groupby(['city','area','building_age_group'])[['使照號碼']].count()
cnt_df.reset_index(inplace = True)
cnt_df = cnt_df.pivot(index = ['city','area'] , columns='building_age_group' , values='使照號碼')
cnt_df.columns = ['未滿10Y','10Y-20Y','21Y-30Y','30Y以上']

# 分組並累計符合年數後計算百分比
cnt_df['over_20y'] = cnt_df['21Y-30Y'] + cnt_df['30Y以上']
cnt_df['over_10y'] = cnt_df['over_20y'] + cnt_df['10Y-20Y']
cnt_df['total'] = cnt_df['未滿10Y'] + cnt_df['10Y-20Y'] + cnt_df['21Y-30Y'] + cnt_df['30Y以上']
cnt_df['30y_percent'] = cnt_df['30Y以上'] / cnt_df['total']
cnt_df['20y_percent'] = cnt_df['over_20y'] / cnt_df['total']
cnt_df['10y_percent'] = cnt_df['over_10y'] / cnt_df['total']
cnt_df['under_10y_percent'] = cnt_df['未滿10Y'] / cnt_df['total']


# In[6]:


# 產出含長條圖的資料圖片檔
style_cnt_df = cnt_df[['under_10y_percent','10y_percent','20y_percent','30y_percent','total']].sort_values('total' , ascending = False)
style_cnt_df.columns = ['未滿10年','10年以上(累計)','20年以上(累計)','30年以上','總發照數']
out_style_cnt_df = style_cnt_df.style.format('{:.2%}' , subset = ['未滿10年','10年以上(累計)','20年以上(累計)','30年以上']).bar(subset = ['未滿10年','10年以上(累計)','20年以上(累計)','30年以上']).set_caption('各區發照數年限分佈')

di.export(out_style_cnt_df,'building_age_distributed.png')
style_cnt_df.to_parquet('REPORT_building_age_distributed.gzip' , compression = 'gzip')

