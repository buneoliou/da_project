#!/usr/bin/env python
# coding: utf-8

# In[1]:


import urllib
import pandas as pd
import datetime
import json
import time
import hashlib
import re
import xmltodict
import requests
from bs4 import BeautifulSoup
import pymongo
import os

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import Select


# In[2]:


def create_docker_mongodb(username , password):
    r = os.system(f'docker run -d --name mongodb 	-e MONGO_INITDB_ROOT_USERNAME={username} 	-e MONGO_INITDB_ROOT_PASSWORD={password} 	mongodb_build')
    if r == 0:
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),f'docker mongodb已建立完成          username = {username} , password = {password} , container_name = mongodb_build')
    else :
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'建立docker失敗')


# In[3]:


def get_city_dict(file_path , executable_path):
    
    '''如果未指定現有city_dict xpath則從網路抓取'''
    
    if file_path != '':
        
        with open(file_path,'r') as f:
            city_dict = eval(f.read())
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'已讀取指定city dict檔案')
        
    else :
        
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'開始從網路抓取資料')
        post_gov_url = 'https://www.post.gov.tw/post/internet/Postal/index.jsp?ID=207'
        
        browser = webdriver.Chrome(executable_path = executable_path, options = chrome_options)     
        # 設定等待時間
        browser.implicitly_wait(10)
        
        browser.get(post_gov_url)
        
        ## 取得"市"級清單
        city_list = browser.find_element(by = 'id' , value = 'city').text
        city_list = city_list.replace(' ','').split("\n")
        city_list = [city for city in city_list if len(city)==3]
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'已取得"市"級清單')
        
        ## 取得"子行政"區清單
        city_dict = {}
    
        for city in city_list:
    
            city_dict[city] = {}
            s = Select(browser.find_element(by = 'id' , value = 'city')).select_by_value(city)
            t = browser.find_element(by = 'id' , value = 'cityarea').text
            t_list = t.replace(' ','').split("\n")
            
            for zone in t_list:
                city_dict[city][zone] = []
            
            time.sleep(1)
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'已取得"子行政區"清單')    
            
        ## 取得各"子行政區"的道路名稱
        post_road_url = 'https://www.post.gov.tw/post/internet/Postal/streetNameData_zip6.jsp?city={0}&cityarea={1}'
    
        for city in city_dict:
            print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),f'開始取得{city}下道路清單')
            for area in city_dict[city]:
                
                
                
                browser.get(post_road_url.format(city,area))
                t = browser.find_element(by = 'tag name' , value = 'body').text
                
                for street in eval(t):
                    city_dict[city][area].append(street['street_name'])
                
                time.sleep(1)
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'已取得"子行政區下道路"清單')
        
    return city_dict


# In[4]:


def get_post_dict(executable_path):
    
    '''取得所有郵遞區號'''
    
    browser = webdriver.Chrome(executable_path = executable_path, options = chrome_options)    
    
    browser.get('https://www.post.gov.tw/post/download/1050812_%E8%A1%8C%E6%94%BF%E5%8D%80%E7%B6%93%E7%B7%AF%E5%BA%A6(toPost).xml')
    xml = browser.page_source
    origin_post_data = xmltodict.parse(xml)['html']['body']['div'][0]['dataroot']['_x0031_050429_行政區經緯度_x0028_toPost_x0029_']
    browser.close()
    
    post_dict = {}    
    for row in origin_post_data:
        post_dict[row['行政區名']] = row['_x0033_碼郵遞區號']
        
    print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'已取得所有3碼郵遞區號清單')    
    return post_dict


# In[5]:


def Get_Building_info(city_dict , post_dict ,  module , executable_path):
    
    '''function  : 取得各行政區、子行政區下各路段建照
       module   ： F => 依傳入city_dict取得資料後，再針對error_log出現路段重新執行
                ： R => 只針對error_log內資料，並整理成程式吃檔格式後重新執行'''
    
    print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'開始取得建照資料')
    
    if module == 'R':
        db.building_error_log.drop()
    
    browser = webdriver.Chrome(executable_path = executable_path , options=chrome_options)

    for city in list(city_dict.keys()):
        
        for area in list(city_dict[city].keys()):
        
            for road in city_dict[city][area]:
                
                browser.get('https://building-management.publicwork.ntpc.gov.tw/bm_query.jsp?rt=3')
                
                # 輸入資料：建造類型(固定為使用建照3)
                A2_e = browser.find_element(by = 'id' , value = 'A2')
                A2_js = 'arguments[0].value = "3"'
                browser.execute_script(A2_js,A2_e)
                
                # 輸入資料：3碼郵遞區號（以city+area查詢post_dict內對應郵遞區號）
                D1_e = browser.find_element(by = 'id' , value = 'D1')
                D1_js = 'arguments.value = "{}"'.format(post_dict[f'{city}{area}'])
                browser.execute_script(D1_js,D1_e)
                
                D3_e = browser.find_element(by = 'id' , value = 'D3')
                
                # 以try方式為避免在網路取得路名selenium輸入路名時出錯，並在出錯時紀錄至mongo.crawler.building_error下
                try :
                    D3_e.send_keys(road)             
                    
                    # 取得該次Java取得驗證碼後輸入至Z1位置
                    code_url = 'https://building-management.publicwork.ntpc.gov.tw/getplaylist.jsp'
                    js = f'window.open("{code_url}")'
                    browser.execute_script(js)
                    window_list = browser.window_handles
                    browser.switch_to.window(window_list[1])
                    browser.get(code_url)
                    browser.page_source
                    val_code = eval(re.findall(r'\{.+\}',browser.page_source)[0])['data']
                    browser.close()
                    browser.switch_to.window(window_list[0])
                    Z1_e = browser.find_element(by = 'id' , value = 'Z1')
                    Z1_e.send_keys(val_code)        
                    botton = browser.find_element(by = 'xpath' , value = '/html/body/div[2]/div/div[2]/section[2]/form/div[2]/table/tbody/tr/td[2]/button')
                    time.sleep(1)
                    ActionChains(browser).click(botton).perform()
                    
                    # 設定五秒以確保資料已完成載入
                    time.sleep(5)
                    
                    # 紀錄該路段資料筆數至mongdb.crawler.building_amt
                    amt = browser.find_element(by = 'xpath' , value = '/html/body/div/div/div[2]/section[2]/span')
                    db.building_amt.insert_one({'city':city , 'area':area , 'road': road , 'amt':amt.text})
                    
                    # 取得總頁數
                    e_page = browser.find_element(by = 'xpath', value = '/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b')
                    
                    # 判斷若無頁數為無資料，將log紀錄至mongdb.crawler.building_log
                    if e_page.text != '':                        
                        page = re.findall(r'.+第(\d+)頁，共(\d+)頁.+',e_page.text.split('\u3000')[3])[0]
                        
                        # 依照總頁數對Table進行資料取得及寫入次數
                        for i in range(int(page[1])):            
                            df = pd.read_html(browser.page_source)[1]
                            
                            # 不寫入Table最後一行，此行為選擇上、下頁無需紀錄
                            for i in df.index[:-1]:            
                                db.building.insert_one({'city':city , 'area':area , 'road': road ,                                                         '使照號碼':df.loc[i,'使照號碼'] , '建照號碼':df.loc[i,'建照號碼'] ,'起造人':df.loc[i,'起造人'],
                                                        '設計人'  :df.loc[i,'設計人']  , '建築地址(代表號)':df.loc[i,'建築地址(代表號)'] , '發照日期':df.loc[i,'發照日期']})
                            
                            botton_next = browser.find_element(by = 'xpath' , value = '/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b/a[3]')
                            ActionChains(browser).click(botton_next).perform()
                            
                            # 設定五秒以確保資料已完成載入
                            time.sleep(5)
                            
                    else :
                        db.building_log.insert_one({'city':city , 'area':area , 'road': road , 'log':'No data', 'datetime' : datetime.datetime.now()})
                except :
                    db.building_error_log.insert_one({'city':city , 'area':area , 'road': road , 'log':'路名錯誤', 'datetime' : datetime.datetime.now()})
               
                print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),f'已取得{city}/{area}/{road}資料')
    browser.close()      
    print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'city_dict內已執行完畢')
    


# In[6]:


def Get_reload_city_dict():
    '''重新對上次進行Get_Building下的error_log整理成dict'''
    
    # 清洗資料、整理
    df = pd.DataFrame(db.building_error_log.find())
    df = df[['city','area','road']].groupby(['city','area','road']).sum()
    df.reset_index(inplace = True)
    df['road'] += ','
    df = df.groupby(['city','area']).sum()
    df = df.road.str.split(',',expand = True)
    df.reset_index(inplace = True)
    df.fillna('', inplace = True)
    
    
    # 將相同city , area 下路名以list儲存，達到與原city_dict格式相同
    re_city_dict = {}
    for i in range(len(df)):
        
        city = df.loc[i,'city']
        area = df.loc[i,'area']
        
        # 判斷dict中是否已存在此city key值
        if city in re_city_dict.keys():
            
            re_city_dict[city][area] = []
            for road in df.iloc[i,2:]:
                if road != '':
                    re_city_dict[city][area].append(road)
                
        else :
            
            re_city_dict[city] = {}
            re_city_dict[city][area] = []
            for road in df.iloc[i,2:]:
                if road != '':
                    re_city_dict[city][area].append(road)
                    
    return re_city_dict


# In[7]:


if __name__ == '__main__':
    
    # 載入Parameters
    param = pd.read_csv('crawler_param.csv' , delimiter= ',' , index_col = 0 , na_values = 'No Default' , keep_default_na = False)
    
    file_path = param.loc['file_path','value']
    MongoDB_connect = param.loc['MongoDB_connect','value']
    module = param.loc['get_data_module','value']
    spec_city = param.loc['spec_city','value']
    create_docker_mongodb = param.loc['create_docker_mongodb','value']
    chromedrive_path = param.loc['chromedrive_path','value']
    
    if create_docker_mongodb == '':
        create_docker_mongodb = 'N'    
    
    if create_docker_mongodb.upper() not in ('Y','N'):
        raise ValueError('create_docker_mongodb參數設定錯誤，無法辨識')
        
    elif create_docker_mongodb.upper() == 'Y':
        
        u = re.findall(r'\/(\w+)\:(\w+)',MongoDB_connect)[0]
        username = u[0]
        password = u[1]
        
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'開始取得建立docker mongodb')
        create_docker_mongodb(username = username , password = password)  
        
    
    # module預設為F
    if module == '':
        module = 'F'
    
    # 檢查module參數
    if module not in ('F','R'):
        raise ValueError('Module參數設定錯誤，無法辨識')
    
    # 設定 Chrome_options參數
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    
    # 建立MongoDB立線
    session = pymongo.MongoClient(MongoDB_connect)    
    db = session.crawler

    if db != None:
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'成功連線至MongoDB')
    else :
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'DB連線失敗')
        
    # 依參數取得目標清單
    city_dict = get_city_dict(executable_path = chromedrive_path , file_path = file_path)   
    
    # 若有指定縣市時，修改成程式可接受dict格式
    if spec_city != '':
        if spec_city not in list(city_dict.keys()):
            raise ValueError(f'指定縣市無法辨識，請參考此列表:\n{list(city_dict.keys())}')
        tmp_dict = {}
        city_dict = city_dict[spec_city]
        tmp_dict[spec_city] = city_dict
        city_dict = tmp_dict  
    
    # 取得3碼郵遞區號
    post_dict = get_post_dict(executable_path = chromedrive_path) 
    
    # 開始取得建照資料
    if module == 'F':
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),"F模式 - 依傳入city_dict取得資料後，再針對error_log出現路段重新執行")
                
        Get_Building_info(city_dict = city_dict , post_dict = post_dict , module = 'F' , executable_path = chromedrive_path)
        reload_city_dict = Get_reload_city_dict()
        Get_Building_info(city_dict = city_dict , post_dict = post_dict , module = 'R' , executable_path = chromedrive_path)
        
    else :
        
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),"R模式 -  只針對error_log內資料，並整理成程式吃檔格式後重新執行")
        Get_Building_info(city_dict = city_dict , post_dict = post_dict , module = 'R' , executable_path = chromedrive_path)
    

