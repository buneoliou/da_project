#!/usr/bin/env python
# coding: utf-8

# In[1]:


from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import datetime
import ddddocr
import base64
import time
import pyautogui
from bs4 import BeautifulSoup


# In[2]:


url = 'http://www.goldenjade.com.tw/captchaCheck/check/imgcheck_form.html'

options = Options()

browser = webdriver.Chrome(options= options)
browser.get(url)


# In[3]:


img = browser.find_element(by = 'xpath' , value = '/html/body/div/form/table/tbody/tr/td[2]/img')
# 執行javascript取得驗證圖片
img_base64 = browser.execute_script("""
    var ele = arguments[0];
    var cnv = document.createElement('canvas');
    cnv.width = ele.width; cnv.height = ele.height;
    cnv.getContext('2d').drawImage(ele, 0, 0);
    return cnv.toDataURL('image/jpeg').substring(22);    
    """, browser.find_element(by = 'xpath' , value = '/html/body/div/form/table/tbody/tr/td[2]/img'))

# 辨識圖片後驗證碼
ocr = ddddocr.DdddOcr()
res = ocr.classification(base64.b64decode(img_base64))

# 輸入驗證碼並送出
input_box = browser.find_element(by = 'xpath' , value = '/html/body/div/form/table/tbody/tr/td[1]/input')
input_box.send_keys(res)
click = browser.find_element(by = 'xpath', value = '/html/body/div/form/p/input[2]')
click.click()


# 點掉跳出視窗
time.sleep(1)
pyautogui.press('enter')
time.sleep(1)
pyautogui.press('enter')

# 利用BeautifulSoup打印出驗證完文字
src = browser.page_source
soup = BeautifulSoup(src , 'html.parser')
text = soup.find_all('p')
print('-'*15 +'驗證結果' +'-'*15)
for t in text:
    print(t.getText())
browser.close()

